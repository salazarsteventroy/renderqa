import React from 'react';
import "../components/product1.css";
import Colors from '../components/Colors'
import DetailsThumb from '../components/DetailsThumb';
import Navbar from "../components/Navbar";
import Navbar2 from "../components/Navbar2";
import Products from "../components/Products";
import Footer from "../components/Footer";
class Product1 extends React.Component{

  state = {
    products: [
      {
        "_id": "1",
        "title": "Cup Sample",
        "src": [
            "https://i.ibb.co/vchYZ6q/COFFEE-MOCK-UP-1.jpg",
            "https://i.ibb.co/vQk2Ztv/COFFEE-MOCK-UP-2.jpg",
            "https://i.ibb.co/X26gqnX/COFFEE-MOCK-UP-3.jpg",
            "https://i.ibb.co/vchYZ6q/COFFEE-MOCK-UP-1.jpg"
          ],
        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
        "price": 50,
        // "colors":["gray","black","green","teal"],
        "count": 1
      }
    ],
    index: 0
  };

  myRef = React.createRef();

  handleTab = index =>{
    this.setState({index: index})
    const images = this.myRef.current.children;
    for(let i=0; i<images.length; i++){
      images[i].className = images[i].className.replace("active", "");
    }
    images[index].className = "active";
  };

  componentDidMount(){
    const {index} = this.state;
    this.myRef.current.children[index].className = "active";
  }


  render(){
    const {products, index} = this.state;
    return(
      <div className="app">
        <Navbar />
        <Navbar2 /> 
        {
 
          products.map(item =>(
            <div className="details" key={item._id}>
              <div className="big-img">
                <img src={item.src[index]} alt=""/>
              </div>
  
              <div className="box">
                <div className="row">
                  <h2>{item.title}</h2>
                  <h4>${item.price}</h4>
                </div>
                {/* <Colors colors={item.colors} /> */}

                <p>{item.description}</p>
                <p>{item.content}</p>

                <DetailsThumb images={item.src} tab={this.handleTab} myRef={this.myRef} />
                <button className="cart">Add to cart</button>

              </div>
            </div>
          ))
        }
        &nbsp;&nbsp;
        <div className="productSection">
        <Products />
        <Footer />
        </div>

      </div>
    );
  };
}

export default Product1;
