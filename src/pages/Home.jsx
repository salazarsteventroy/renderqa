import React from "react";
import Categories from "../components/Categories";
import Footer from "../components/Footer";
import Navbar from "../components/Navbar";
import Newsletter from "../components/Newsletter";
import Products from "../components/Products";
import Slider from "../components/Slider";
import Navbar2 from "../components/Navbar2";
import Highlight from "../components/Highlight";


const Home = () => {
  return (
    <div>
      <Navbar />
      <Navbar2 />
      <Slider />
      <Categories />
      <Products/>
      <Highlight/>
      <Newsletter/>
      <Footer/>
    </div>
  );
};


export default Home;
