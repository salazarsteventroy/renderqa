export const sliderItems = [
    {
      id: 1,
      img: "https://images.pexels.com/photos/2065650/pexels-photo-2065650.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
      title: "WEIRDLY MEANINGFUL ART",
      desc: "Millions of designs on over 70 high quality products",
      bg: "F8C8DC",
    },
  
    {
      id: 2,
      img: "https://images.pexels.com/photos/187334/pexels-photo-187334.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
      title: "PURCHASES PAY ARTISTS",
      desc: "Money goes directly into a creative person's pocket",
      bg: "A7C7E7",
    },
    {
      id: 3,
      img: "https://images.pexels.com/photos/68558/pexels-photo-68558.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
      title: "SOCIALLY RESPONSIBLE PRODUCTION",
      desc: "We're investing in programs to offset all carbon emissions",
      bg: "C1E1C1",
    },
  ];

  export const categories = [
    {
      id: 1,
      img: "https://images.pexels.com/photos/1793350/pexels-photo-1793350.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
      title: "BOXES!",
    },
    {
      id: 2,
      img: "https://images.pexels.com/photos/7119970/pexels-photo-7119970.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
      title: "PAPER BAGS!",
    },
    {
      id: 3,
      img: "https://images-ext-1.discordapp.net/external/JE1GaG41JlKalzvFEk7jlqKVrol57Ld2PdB3wkca5_Q/%3Fcs%3Dsrgb%26dl%3Dpexels-imso-gabriel-5798314.jpg%26fm%3Djpg/https/images.pexels.com/photos/5798314/pexels-photo-5798314.jpeg?width=720&height=477",
      title: "BOOKLETS!",
    },
  ];

  export const popularProducts = [
    {
      id:1,
      img:"https://i.ibb.co/pKWpCLD/1.png",
      bg: "green"
    },
    {
      id:2,
      img:"https://i.ibb.co/qjLps7Q/2.png",
    },
    {
      id:3,
      img:"https://i.ibb.co/xsMngD4/3.png",
    },
    {
      id:4,
      img:"https://i.ibb.co/2YPN8NB/4.png",
    },
    {
      id:5,
      img:"https://i.ibb.co/nQRchvd/5.png",
    },
    {
      id:6,
      img:"https://i.ibb.co/HVMHn9C/6.png",
    },
    {
      id:7,
      img:"https://i.ibb.co/vchYZ6q/COFFEE-MOCK-UP-1.jpg",
    },
    {
      id:8,
      img:"https://i.ibb.co/HVMHn9C/6.png",
    },
  ]

export const highLights = [
    {
      id:1,
      img:"https://images.pexels.com/photos/4270847/pexels-photo-4270847.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
    }
  ]



  