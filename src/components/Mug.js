import React, { useRef } from 'react'
import { useGLTF } from '@react-three/drei'


export default function Model({ ...props }) {
  const group = useRef()
  const { nodes, materials } = useGLTF('/mug.gltf')
  return (
    <group ref={group} {...props} dispose={null}>
      <group rotation={[-Math.PI / 2, 0, 0]}>
        <group rotation={[Math.PI / 2, 0, 0]}>
          <group rotation={[-Math.PI / 2, 0, 0]} scale={1}>
            <mesh geometry={nodes.Cylinder_Material001_0.geometry} material={materials['Material.001']} />
          </group>
        </group>
      </group>
    </group>
  )
}

useGLTF.preload('/mug.gltf')
