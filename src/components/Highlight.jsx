import React from "react";
import styled from "styled-components";
import { mobile } from "../responsive";
// import Container from "react-bootstrap/esm/Container";
import Mug from "../components/Mug";
// import Box from "../components/Box";
import { Canvas } from "@react-three/fiber";
import { OrbitControls } from "@react-three/drei";




const Container = styled.div`
  display: flex;
`

const Wrapper = styled.div`
  padding: 5px 20px 20px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  position: relative;
  ${mobile({ padding: "10px 0px" })}
  background-image: url("https://images.unsplash.com/photo-1494438639946-1ebd1d20bf85?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1167&q=80");
  background-color: #7C8C9C;
  
`
  
;

const Left = styled.div`
  flex: 1.5;
  margin-left: 50px;
  align-items: center;
  justify-content: flex-start;
  ${mobile({ flex: 2, justifyContent: "center" })}
  
`;


const Right = styled.div`
  flex: 1.5;
  align-items: center;
  justify-content: flex-end;
  background-color: #DBE0EC;
  height: 60vh;
  width: auto;
  ${mobile({ flex: 2, justifyContent: "center" })}
  margin-right: 15px;
  margin-top: 10px;
  canvas {
    height: ;
  }
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
  
`


const Title = styled.h1`
  font-size: 40px;
  margin-bottom: 50px;
  justify-content: flex-start;
  flex-wrap: wrap;
`;
const List = styled.ul`
  margin: 0;
  padding: 0;
  list-style: none;
  display: flex;
  flex-wrap: wrap;
`;

const ListItem = styled.li`
  width: 50%;
  margin-left: 150px;
  margin-bottom: 60px;
  font-size: 20px
  
`;


const Button = styled.button`
margin-left: 15px;
margin-top: 15px;
border:none;
padding: 10px;
background-color: #F3F3F4;
color:gray;
cursor: pointer;
font-weight: 600;
border-radius: 30%;
box-shadow: 3px 10px 5px grey;
`



function Highlight() {
  return (
    <Container>
        <Wrapper>
            <Left>
                <List>
                <Title>Products featuring the thing you love made by an artist who loves it too.</Title>
                    <ListItem>An artist creates a design and uploads it.</ListItem>
                    <ListItem>You find a product featuring that design.
                    </ListItem>
                    <ListItem>The product is responsibly produced and shipped.</ListItem>
                </List>                
            </Left>
            <Right>

            <Button>style1</Button>
            <Button>style2</Button> 
            <Button>style3</Button>
            <Button>style4</Button>
            <Canvas>
              <OrbitControls enableZoom = {true} />
              <ambientLight intensity = {0.5} />
              <directionalLight position={[-2, 5, 2]} intensity = {1} />
              <Mug/>
            </Canvas>
            </Right>
        </Wrapper>
    </Container>
  )
}

export default Highlight;