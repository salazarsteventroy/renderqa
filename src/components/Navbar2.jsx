import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';



function Navbar2() {
  return (
    <Navbar bg="light" expand="lg">
      <Container class="d-flex justify-content-center">
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <NavDropdown title="Demo Products" id="basic-nav-dropdown">
              <NavDropdown.Item href="#action/3.1">Commercial Printing</NavDropdown.Item>
              <NavDropdown.Item href="#action/3.2">
                Wide Format
              </NavDropdown.Item>
              <NavDropdown.Item href="#action/3.3">Photo Products</NavDropdown.Item>
              <NavDropdown.Divider />
              <NavDropdown.Item href="#action/3.4">
                Others..
              </NavDropdown.Item>
            </NavDropdown>
            <Nav.Link href="#link1">HeaderCards</Nav.Link>
            <Nav.Link href="#link2">Bookmarks</Nav.Link>
            <Nav.Link href="#link3">Brochures/Flyers</Nav.Link>
            <Nav.Link href="#link5">Envelopes</Nav.Link>
            <Nav.Link href="#link6">Booklets</Nav.Link>
            <Nav.Link href="#link7">Apparels</Nav.Link>
            <Nav.Link href="#link8">WallCalendar</Nav.Link>
            <Nav.Link href="#link9">Letterheads</Nav.Link>
            <Nav.Link href="#link10">NCRCarbonless</Nav.Link>
            <Nav.Link href="#link11">Postcards</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default Navbar2;