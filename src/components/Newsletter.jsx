import { Send } from "@material-ui/icons";
import styled from "styled-components";
import { mobile } from "../responsive";
import {Map, InfoWindow, Marker, GoogleApiWrapper} from 'google-maps-react';
import AddressMap from "./Map";

const Container = styled.div`
  height: 50vh;
  background-color: #F3F3F4;
  
  display: flex;
  margin-top: 30px;
`;

const Left = styled.div`
  flex: 1.5;
  margin-left: 50px;
  align-items: center;
  justify-content: flex-start;
  margin-top: 150px;
`

const Right = styled.div`
flex: 1.5;
align-items: center;
justify-content: flex-end;
background-color: #fcf5f5;
height: auto;
width: auto;
margin: 30px;
box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
`
const MapContainer = styled.div`
  height: 100px;
  width: 300px;

`

const Title = styled.h1`
  font-size: 70px;
  margin-bottom: 20px;
`;

const TitleTwo = styled.h1`
  font-size: 30px;
  margin-bottom: 20px;
  text-align: center;
`;

const Desc = styled.div`
  font-size: 24px;
  font-weight: 300;
  margin-bottom: 20px;
  ${mobile({ textAlign: "center" })}

`;

const InputContainer = styled.div`
  width: 50%;
  height: 40px;
  background-color: white;
  display: flex;
  justify-content: space-between;
  border: 1px solid lightgray;
  ${mobile({ width: "80%" })}
`;

const Input = styled.input`
  border: none;
  flex: 8;
  padding-left: 20px;
`;

const Button = styled.button`
  flex: 1;
  border: none;
  background-color: teal;
  color: white;
`;

const Newsletter = () => {
  return (
    <Container>
      <Left>
      <Title>Newsletter</Title>
      <Desc>Get timely updates from your favorite products.</Desc>
      <InputContainer>
        <Input placeholder="Your email" />
        <Button>
          <Send />
        </Button>
      </InputContainer>
      </Left>
      <Right>
      {/* <MapContainer>
        <AddressMap/>
        </MapContainer> */}
      </Right>

    </Container>
  );
};

export default Newsletter
